<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Log_Model extends MY_Model {

    private $table_input = "log_input";
    private $table_users = "log_users";

    function insert_log($unique_id, $action, $data, $value=NULL)
    {
        $sql = "INSERT INTO `" . $this->table_input . "` (`id_log` ,`ip` ,`unique_id` ,`action` ,`data`, `value` ,`time`)VALUES (NULL ,  ' ',  '" . $unique_id . "',  '" . $action . "',  '" . $data . "','" . $value . "', CURRENT_TIMESTAMP);";
        $this->db->query($sql);
        $query = $this->db->insert_id();
        return $query;
    }

    function insert_login_users($unique_id)
    {
        $sql = "INSERT INTO  `".$this->table_users."` (`id_log` ,`id_user` ,`action` ,`date`)VALUES (NULL ,  '".$unique_id."',  'login',CURRENT_TIMESTAMP);";
        $this->db->query($sql);
        $query = $this->db->insert_id();
        return $query;
    }

    function insert_logout_users($unique_id)
    {
        $sql = "INSERT INTO  `".$this->table_users."` (`id_log` ,`id_user` ,`action` ,`date`)VALUES (NULL ,  '".$unique_id."',  'logout',CURRENT_TIMESTAMP);";
        $this->db->query($sql);
        $query = $this->db->insert_id();
        return $query;
    }

    function get_ip()
    {
        $ipaddress = '';
        if ($_SERVER['HTTP_CLIENT_IP'])
            $ipaddress = @$_SERVER['HTTP_CLIENT_IP'];
        else if ($_SERVER['HTTP_X_FORWARDED_FOR'])
            $ipaddress = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        else if ($_SERVER['HTTP_X_FORWARDED'])
            $ipaddress = @$_SERVER['HTTP_X_FORWARDED'];
        else if ($_SERVER['HTTP_FORWARDED_FOR'])
            $ipaddress = @$_SERVER['HTTP_FORWARDED_FOR'];
        else if ($_SERVER['HTTP_FORWARDED'])
            $ipaddress = @$_SERVER['HTTP_FORWARDED'];
        else if ($_SERVER['REMOTE_ADDR'])
            $ipaddress = @$_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

}