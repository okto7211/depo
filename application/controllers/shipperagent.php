<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Shipperagent extends CI_Controller {

    public function __construct()
    {

        parent::__construct();

        $this->load->model('Shipperagent_Model');
        $this->load->library('lib_auth');
        $this->load->model('Log_Model');
    }

    public function index()
    {
        $this->lib_auth->check('yes');
        $data['page_title'] = APP_NAME . " | All Shipper Agents";

        if (isset($_GET['hal']))
            $hal = $_GET['hal'];
        else
            $hal = '';

        $dataPerhalaman = 10;
        ($hal == '') ? $nohalaman = 1 : $nohalaman = $hal;
        $offset = ($nohalaman - 1) * $dataPerhalaman;
        $off = abs((int) $offset);
        
        $jumlahData = count($this->Shipperagent_Model->get_all_data("shipping_agents"));
        
        $data['paginator'] = $this->Shipperagent_Model->page($jumlahData, $dataPerhalaman, $hal);
        
        $data['datas'] = $this->Shipperagent_Model->get_all_data_tabel("shipping_agents", $dataPerhalaman, $off);

        $this->load->view('data/all_shippingagent_v', $data);
    }

    public function add_new()
    {
        $data['page_title'] = APP_NAME . " | Add New Shipper Agents";

        $this->load->library('form_validation');

        $this->load->view('data/add_new_shipperagent_v', $data);
    }

    public function save()
    {
        $input = array(
            "id" => str_replace(" ", "", strtoupper($this->input->post("id"))),
            "name" => $this->input->post("name"),
            "creator" => $_SESSION[SESSION_NAME]['unique_id'],
            "created" =>date("Y-m-d H:i:s"),
            "line_id"=>""
        );
        $edit = $this->input->post("edit");
        if ($edit)
        {
            $id = $this->input->post("id");
            $record = $this->Shipperagent_Model->update("shipping_agents", $id, $input, "id");

            redirect(base_url() . "shipperagent");
        }
        else
        {

            $record = $this->Shipperagent_Model->insert("shipping_agents", $input);
            if ($record)
            {
                redirect(base_url() . "shipperagent");
            }
        }
            redirect(base_url() . "shipperagent");
    }

    public function delete()
    {
        $id_product = $this->uri->segment(3, '');

        if (isset($id_product))
        {
            $product = $this->Shipperagent_Model->delete("shipping_agents", str_replace("_", " ", $id_product), "id");

            if ($product)
            {
                $this->Shipperagent_Model->delete("shipping_agents", $id_product, "id");

            }
        }
                redirect(base_url() . "shipperagent");
    }

    public function view()
    {
        $id_product = $this->uri->segment(3, '');

        $data['edit'] = TRUE;
        if (isset($id_product))
        {
            $data['datas'] = $this->Shipperagent_Model->get_single("shipping_agents", str_replace("_", " ", $id_product), "id");
        }
        else
        {
            redirect(base_url() . "shipperagent");
        }
        $data['page_title'] = APP_NAME . " | Edit Shipper Agents";

        $this->load->library('form_validation');

        $this->load->view('data/edit_shippingagent_v', $data);
    }

}