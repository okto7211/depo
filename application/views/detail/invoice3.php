<?php 
 function Terbilang($a) {
    $ambil = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    if ($a < 12)
        return " " . $ambil[$a];
    elseif ($a < 20)
        return Terbilang($a - 10) . "belas";
    elseif ($a < 100)
        return Terbilang($a / 10) . " puluh" . Terbilang($a % 10);
    elseif ($a < 200)
        return " seratus" . Terbilang($a - 100);
    elseif ($a < 1000)
        return Terbilang($a / 100) . " ratus" . Terbilang($a % 100);
    elseif ($a < 2000)
        return " seribu" . Terbilang($a - 1000);
    elseif ($a < 1000000)
        return Terbilang($a / 1000) . " ribu" . Terbilang($a % 1000);
    elseif ($a < 1000000000)
        return Terbilang($a / 1000000) . " juta" . Terbilang($a % 1000000);
}
?>
<body onLoad="javascript:window.print()">
	<table width="834" border="0" align="center" style="font-size:12px;font-family:Tahoma, Geneva, sans-serif">
        <tr>
            <td><div align="left">KONSORSIUM PT.SUCOFINDO (Persero)</div></td>
            <td><div align="right"><?= date("d/m/y"); ?></div></td>
        </tr>
    </table>
    <table width="200" border="" align="center" style="font-size:12px;font-family:Tahoma, Geneva, sans-serif">
        <tr>
            <td><div align="center">OFFICIAL RECEIPT</div></td>
        </tr>
    </table>
    <table width="834" border="0" align="center" style="font-size:12px;font-family:Tahoma, Geneva, sans-serif">
        <tr>
            <td width="149">Trx Id</td>
            <td width="10">:</td>
            <td width="199"><?= $trx_id; ?></td>
            <td width="164">No Polisi</td>
            <td width="10">:</td>
            <td width="85">&nbsp;</td>
        </tr>
        <tr>
            <td>Tax ID</td>
            <td>:</td>
            <td><?= $trans['customer_tax_id']; ?></td>
            <td>Customer</td>
            <td>:</td>
            <td><?= $trans['customer_name']; ?></td>
        </tr>
        <tr>
            <td>Type</td>
            <td>: </td>
            <td><?= $trans['trx_type_id']; ?></td>
            <td> Depo</td>
            <td>:</td>
            <td><?= $trans['depo_id']; ?></td>
        </tr>
        <tr>
            <td>Document Number</td>
            <td>:</td>
            <td><?= $trans['doc_number']; ?></td>
            <td>Vessel</td>
            <td>:</td>
            <td><?= $trans['vessel_id']; ?></td>
        </tr>
        <tr>
            <td>Agent</td>
            <td>:</td>
            <td><?= $trans['shipping_agent_id']; ?></td>
            <td>Truck Co</td>
            <td>:</td>
            <td><?= $trans['trucking_company_id']; ?></td>
        </tr>
    </table>
    <?php
    $index = 1;
    $sub_total = 0;
    $box20 = 0;
    $box40 = 0;
    $total = 0;
    $total20 = 0;
    $total40 = 0;
    $total_all = 0;
    $total_all2 = 0;
    foreach ($equip as $v)
    {
        if ($v['eq_size'] == "20")
        {
            $box20++;
        }
        else
        {
            $box40++;
        }
        $total++;
    }
    $total20 = $box20 * 75000;
    $total40 = $box40 * 100000;
    $total_all = $total20 + $total40;
    $total_all2 = $total_all + (($total_all * 10) / 100);
    ?>
    <hr width="820px">
    <table width="832" border="0" align="center" style="font-size:12px;font-family:Tahoma, Geneva, sans-serif">
        <tr>
            <td width="106">Recieved From :</td>
            <td width="710"><?= $trans['customer_name']; ?></td>
        </tr>
        <tr>
            <td>The sum of : </td>
            <td><?php echo ucwords(Terbilang($total_all2)); ?></td>
        </tr>
    </table>
<table width="832" border="1" align="center" style="font-size:12px;font-family:Tahoma, Geneva, sans-serif">
        <tr>
            <td>Container</td>
            <td style="text-align: center">Qty</td>
            <td style="text-align: center">Price/Unit</td>
            <td style="text-align: center">Subtotal</td>
        </tr>
        <tr>
            <td>20feet</td>
            <td style="text-align: center"><?= $box20; ?></td>
            <td style="text-align: center">75000</td>
            <td style="text-align: center"><?= $total20; ?></td>
        </tr>
        <tr>
            <td>40feet</td>
            <td style="text-align: center"><?= $box40; ?></td>
            <td style="text-align: center">100000</td>
            <td style="text-align: center"><?= $total40; ?></td>
        </tr>
        <tr>
            <td>Total Box</td>
            <td style="text-align: center"><?= $total; ?></td>
            <td style="text-align: center">Total</td>
            <td style="text-align: center"><?= $total_all; ?></td>
        </tr>
        <tr>
            <td colspan="2" rowspan="2">&nbsp;</td>
            <td style="text-align: center">Tax</td>
            <td style="text-align: center">10%</td>
        </tr>
        <tr>
            <td style="text-align: center">Total After Tax</td>
            <td style="text-align: center"><?= $total_all2; ?></td>
        </tr>
    </table>
    <table width="836" height="40" border="1" align="center" style="font-size:12px;font-family:Tahoma, Geneva, sans-serif">
        <tr>
            <td width="373" height="34">Detail :<br>
                  <?php
                            $index = 1;
                            $sub_total = 0;
                            foreach ($equip as $v)
                            {
                                ?>
                  <?= $v['eq_nbr']; ?>
                  ,
                  <?php
                            }
                            ?></td>
            <td width="222"><p>Customer : <br>
          <?= $trans['customer_name']; ?>
          </p></td>
            <td width="227"><p>Operator Pemeriksa :</p></td>
      </tr>
    </table>
</body>