<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Equipment extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Transaction_Model');
        $this->load->model('Depo_Model');
        $this->load->model('Customer_Model');
        $this->load->model('Vessel_Model');
        $this->load->model('Truck_Model');
        $this->load->model('Shipperagent_Model');
        $this->load->model('Shippingline_Model');
        $this->load->model('Isocode_Model');
        $this->load->model('Equipment_Model');
        $this->load->model('Invoice_Model');
        $this->load->library('lib_auth');
        $this->load->model('Log_Model');
    }

    public function index()
    {
        $this->lib_auth->check('yes');

        $data['page_title'] = APP_NAME . " | All Transaction";

        $find = $this->input->get("eq_nbr");
        if (!empty($find))
        {
            $data['datas'] = $this->Equipment_Model->get_datae("equipment_uses", $find, "eq_nbr");
        }
        else
        {
            $sort= $this->input->get("sort");
            $by = $this->input->get("by");
            if (empty($sort) || empty($by))
            {
                $data['datas'] = $this->Equipment_Model->get_all_data("equipment_uses");
            }
            else
            {
                $data['datas'] = $this->Equipment_Model->get_equipment_by_sort($sort, $by);
            }
        }
        $this->load->view('data/all_equipment_v', $data);
    }
    
    public function view()
    {
        $id_product = $this->uri->segment(3, '');

        $data['edit'] = TRUE;
        if (isset($id_product))
        {
            $data['container'] = $this->Equipment_Model->get_single("equipment_uses", $id_product, "eq_nbr");
            $data['damage'] = $this->Equipment_Model->get_datas("equipment_damages", $id_product, "eq_nbr");
            $data['foto'] = $this->Equipment_Model->get_single("equipment_foto", $id_product, "eq_nbr");
            $data['side'] = array("topside","bottomside","rearside", "frontside", "rightside", "leftside", "inside");
        }
        else
        {
            redirect(base_url() . "transaction");
        }
        $data['page_title'] = APP_NAME . " | Edit Equipment";

        $this->load->library('form_validation');

        $this->load->view('data/edit_equipment_v', $data);
    }

    public function print_eir()
    {
        $data['eq_nbr'] = $this->input->get("eq_nbr");
        $data['trans'] = $this->Equipment_Model->get_single("equipment_uses", $this->input->get("eq_nbr"), "eq_nbr");
        $data['equip'] = $this->Equipment_Model->get_datas("equipment_damages", $this->input->get("eq_nbr"), "eq_nbr");

        $dat = $this->Equipment_Model->get_single("equipment_uses", $this->input->get("eq_nbr"), "eq_nbr");
        $data['transaction'] = $this->Transaction_Model->get_single("transaction", $dat['trx_id'], "trx_id");

        $eirr = $this->Equipment_Model->get_single("print_eir", $dat['trx_id'], "trx_id");
        if (!$eirr)
        {
            $eir = array(
                "invoice_nbr" => date("Ymd") . $dat['trx_id'],
                "eq_nbr" => $this->input->get("eq_nbr"),
                "trx_id" => $dat['trx_id'],
                "complete" => "Y",
                "equse_gkey" => $dat['gkey'],
                "created" => date("Y-m-d H:i:s"),
                "creator" => $_SESSION[SESSION_NAME]['unique_id']
            );
            $this->Invoice_Model->insert("print_eir", $eir);
            
            $this->Equipment_Model->update("equipment_uses", $this->input->get("eq_nbr"), array("print_eir_complete" => "Y"), "eq_nbr");
        
//            $this->Log_Model->insert_log($_SESSION[SESSION_NAME]['uid'], "print", "eir", $eir);
        }else{
			$this->Equipment_Model->update("equipment_uses", $this->input->get("eq_nbr"), array("print_eir_complete" => "Y"), "eq_nbr");
		}
        $this->load->view('detail/eir', $data);
    }

    public function payment_realease()
    {
        $trx_id = $this->input->get("trx_id");
        $cek = $this->Transaction_Model->get_single("transaction", $trx_id, "trx_id");

        $det = $this->Equipment_Model->get_datas("equipment_uses", $cek['trx_id'], "trx_id");
        foreach ($det as $v)
        {
            $this->Equipment_Model->update("equipment_uses", $trx_id, array("payment_release" => "Y"), "trx_id");
        }

        $this->Transaction_Model->update("transaction", $trx_id, array("payment_release" => "Y"), "trx_id");

//        $this->Log_Model->insert_log($_SESSION[SESSION_NAME]['uid'], "payment", "transaction", $trx_id);
        redirect(base_url() . "transaction/view/" . $trx_id);
    }
}