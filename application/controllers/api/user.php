<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once ( APPPATH . '/libraries/REST_Controller.php');

class User extends REST_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model("User_Model", "", TRUE);
    }

    public function cek_login_get()
    {
        $username = $this->get('unique_id');
        $password = $this->get('encrypted_password');

        if (!isset($username) || empty($username)) {
            $this->response(array("status" => 0, "error" => "Paramater is not valid 'unique_id'"));
            return;
        }

        if (!isset($password) || empty($password)) {
            $this->response(array("status" => 0, "error" => "Paramater is not valid 'encrypted_password'"));
            return;
        }

/*        $encriptionPassword = new \PyramidLib\helper\Authentication(SECRET_KEY);
        $passwordEncrypt = $encriptionPassword->encrypt_rijndael_256($password);
        $encriptionPassword->close();
        $hasil = $this->User_Model->cek_login($username, $passwordEncrypt);*/
        $hasil = $this->User_Model->cek_login($username, $password);

        if ($hasil->status === 1) {
            $this->response(array("status_login" => 1, "data" => $hasil->user));
        } else {
            $this->response(array("status_login" => 0, "error" => $hasil->error));
        }
    }

}

/* End of file user.php */
/* Location: ./application/controllers/api/user.php */
