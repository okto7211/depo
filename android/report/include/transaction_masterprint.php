<?php
include_once(getabspath("include/transaction_settings.php"));

function DisplayMasterTableInfo_transaction($params)
{
	$detailtable=$params["detailtable"];
	$keys=$params["keys"];
	global $conn,$strTableName;
	$xt = new Xtempl();
	
	$oldTableName=$strTableName;
	$strTableName="transaction";

//$strSQL = "SELECT  trx_id,  depo_id,  trx_type_id,  doc_number,  vessel_name,  vessel_voyage_id,  vessel_id,  customer_name,  shipping_agent_id,  shipping_agent_name  FROM `transaction`  ";

$sqlHead="SELECT trx_id,  depo_id,  trx_type_id,  doc_number,  vessel_name,  vessel_voyage_id,  vessel_id,  customer_name,  shipping_agent_id,  shipping_agent_name";
$sqlFrom="FROM `transaction`";
$sqlWhere="";
$sqlTail="";

$where="";

global $page_styles, $page_layouts, $page_layout_names, $container_styles;
$layout = new TLayout("masterprint","BoldOrange","MobileOrange");
$layout->blocks["bare"] = array();
$layout->containers["0"] = array();

$layout->containers["0"][] = array("name"=>"masterprintheader","block"=>"","substyle"=>1);


$layout->skins["0"] = "empty";
$layout->blocks["bare"][] = "0";
$layout->containers["mastergrid"] = array();

$layout->containers["mastergrid"][] = array("name"=>"masterprintfields","block"=>"","substyle"=>1);


$layout->skins["mastergrid"] = "grid";
$layout->blocks["bare"][] = "mastergrid";$page_layouts["transaction_masterprint"] = $layout;


if($detailtable=="invoice")
{
		$where.= GetFullFieldName("trx_id")."=".make_db_value("trx_id",$keys[1-1]);
	
}
if($detailtable=="equipment_uses")
{
		$where.= GetFullFieldName("trx_id")."=".make_db_value("trx_id",$keys[1-1]);
	
}
if(!$where)
{
	$strTableName=$oldTableName;
	return;
}
	$str = SecuritySQL("Export");
	if(strlen($str))
		$where.=" and ".$str;
	
	$strWhere=whereAdd($sqlWhere,$where);
	if(strlen($strWhere))
		$strWhere=" where ".$strWhere." ";
	$strSQL= $sqlHead.' '.$sqlFrom.$strWhere.$sqlTail;

//	$strSQL=AddWhere($strSQL,$where);

	LogInfo($strSQL);
	$rs=db_query($strSQL,$conn);
	$data=db_fetch_array($rs);
	if(!$data)
	{
		$strTableName=$oldTableName;
		return;
	}
	$keylink="";
	$keylink.="&key1=".htmlspecialchars(rawurlencode(@$data["trx_id"]));
	


//	trx_id - 
			$value="";
				$value = ProcessLargeText(GetData($data,"trx_id", ""),"field=trx%5Fid".$keylink,"",MODE_PRINT);
			$xt->assign("trx_id_mastervalue",$value);

//	depo_id - 
			$value="";
				$value = ProcessLargeText(GetData($data,"depo_id", ""),"field=depo%5Fid".$keylink,"",MODE_PRINT);
			$xt->assign("depo_id_mastervalue",$value);

//	trx_type_id - 
			$value="";
				$value = ProcessLargeText(GetData($data,"trx_type_id", ""),"field=trx%5Ftype%5Fid".$keylink,"",MODE_PRINT);
			$xt->assign("trx_type_id_mastervalue",$value);

//	doc_number - 
			$value="";
				$value = ProcessLargeText(GetData($data,"doc_number", ""),"field=doc%5Fnumber".$keylink,"",MODE_PRINT);
			$xt->assign("doc_number_mastervalue",$value);

//	vessel_name - 
			$value="";
				$value = ProcessLargeText(GetData($data,"vessel_name", ""),"field=vessel%5Fname".$keylink,"",MODE_PRINT);
			$xt->assign("vessel_name_mastervalue",$value);

//	vessel_voyage_id - 
			$value="";
				$value = ProcessLargeText(GetData($data,"vessel_voyage_id", ""),"field=vessel%5Fvoyage%5Fid".$keylink,"",MODE_PRINT);
			$xt->assign("vessel_voyage_id_mastervalue",$value);

//	vessel_id - 
			$value="";
				$value = ProcessLargeText(GetData($data,"vessel_id", ""),"field=vessel%5Fid".$keylink,"",MODE_PRINT);
			$xt->assign("vessel_id_mastervalue",$value);

//	customer_name - 
			$value="";
				$value = ProcessLargeText(GetData($data,"customer_name", ""),"field=customer%5Fname".$keylink,"",MODE_PRINT);
			$xt->assign("customer_name_mastervalue",$value);

//	shipping_agent_id - 
			$value="";
				$value = ProcessLargeText(GetData($data,"shipping_agent_id", ""),"field=shipping%5Fagent%5Fid".$keylink,"",MODE_PRINT);
			$xt->assign("shipping_agent_id_mastervalue",$value);

//	shipping_agent_name - 
			$value="";
				$value = ProcessLargeText(GetData($data,"shipping_agent_name", ""),"field=shipping%5Fagent%5Fname".$keylink,"",MODE_PRINT);
			$xt->assign("shipping_agent_name_mastervalue",$value);
	$xt->display("transaction_masterprint.htm");
	$strTableName=$oldTableName;

}

?>