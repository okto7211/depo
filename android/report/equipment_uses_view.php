<?php 
@ini_set("display_errors","1");
@ini_set("display_startup_errors","1");

include("include/dbcommon.php");
include("include/equipment_uses_variables.php");

add_nocache_headers();
//	check if logged in
if(!@$_SESSION["UserID"] || !CheckSecurity(@$_SESSION["_".$strTableName."_OwnerID"],"Search"))
{ 
	$_SESSION["MyURL"]=$_SERVER["SCRIPT_NAME"]."?".$_SERVER["QUERY_STRING"];
	header("Location: login.php?message=expired"); 
	return;
}

$layout = new TLayout("view2","BoldOrange","MobileOrange");
$layout->blocks["top"] = array();
$layout->skins["pdf"] = "empty";
$layout->blocks["top"][] = "pdf";
$layout->containers["view"] = array();

$layout->containers["view"][] = array("name"=>"viewheader","block"=>"","substyle"=>2);


$layout->containers["view"][] = array("name"=>"wrapper","block"=>"","substyle"=>1);


$layout->containers["fields"] = array();

$layout->containers["fields"][] = array("name"=>"viewfields","block"=>"","substyle"=>1);


$layout->containers["fields"][] = array("name"=>"viewbuttons","block"=>"","substyle"=>2);


$layout->skins["fields"] = "fields";

$layout->skins["view"] = "1";
$layout->blocks["top"][] = "view";
$layout->skins["details"] = "empty";
$layout->blocks["top"][] = "details";$page_layouts["equipment_uses_view"] = $layout;




include('include/xtempl.php');
include('classes/runnerpage.php');
include("classes/searchclause.php");
$xt = new Xtempl();


$query = $gQuery->Copy();

$filename = "";	
$message = "";
$key = array();
$next = array();
$prev = array();
$all = postvalue("all");
$pdf = postvalue("pdf");
$mypage = 1;

//Show view page as popUp or not
$inlineview = (postvalue("onFly") ? true : false);

//If show view as popUp, get parent Id
if($inlineview)
	$parId = postvalue("parId");
else
	$parId = 0;

//Set page id	
if(postvalue("id"))
	$id = postvalue("id");
else
	$id = 1;

//$isNeedSettings = true;//($inlineview && postvalue("isNeedSettings") == 'true') || (!$inlineview);	
	
// assign an id			
$xt->assign("id",$id);

//array of params for classes
$params = array("pageType" => PAGE_VIEW, "id" =>$id, "tName"=>$strTableName);
$params["xt"] = &$xt;
//Get array of tabs for edit page
$params['useTabsOnView'] = useTabsOnView($strTableName);
if($params['useTabsOnView'])
	$params['arrViewTabs'] = GetViewTabs($strTableName);
$pageObject = new RunnerPage($params);

// SearchClause class stuff
$pageObject->searchClauseObj->parseRequest();
$_SESSION[$strTableName.'_advsearch'] = serialize($pageObject->searchClauseObj);

// proccess big google maps

// add button events if exist
$pageObject->addButtonHandlers();

//For show detail tables on master page view
$dpParams = array();
if($pageObject->isShowDetailTables && !isMobile())
{
	$ids = $id;
	$pageObject->jsSettings['tableSettings'][$strTableName]['dpParams'] = array();
}


//	Before Process event
if($eventObj->exists("BeforeProcessView"))
	$eventObj->BeforeProcessView($conn);

$strWhereClause = '';
$strHavingClause = '';
if(!$all)
{
//	show one record only
	$keys=array();
	$strWhereClause="";
	$keys["gkey"]=postvalue("editid1");
	$strWhereClause = KeyWhere($keys);
	$strSQL = gSQLWhere($strWhereClause);
}
else
{
	if ($_SESSION[$strTableName."_SelectedSQL"]!="" && @$_REQUEST["records"]=="") 
	{
		$strSQL = $_SESSION[$strTableName."_SelectedSQL"];
		$strWhereClause=@$_SESSION[$strTableName."_SelectedWhere"];
	}
	else
	{
		$strWhereClause=@$_SESSION[$strTableName."_where"];
		$strHavingClause=@$_SESSION[$strTableName."_having"];
		$strSQL=gSQLWhere($strWhereClause,$strHavingClause);
	}
//	order by
	$strOrderBy=$_SESSION[$strTableName."_order"];
	if(!$strOrderBy)
		$strOrderBy=$gstrOrderBy;
	$strSQL.=" ".trim($strOrderBy);
}

$strSQLbak = $strSQL;
if($eventObj->exists("BeforeQueryView"))
	$eventObj->BeforeQueryView($strSQL,$strWhereClause);
if($strSQLbak == $strSQL)
{
	$strSQL=gSQLWhere($strWhereClause,$strHavingClause);
	if($all)
	{
		$numrows=gSQLRowCount($strWhereClause,$strHavingClause);
		$strSQL.=" ".trim($strOrderBy);
	}
}
else
{
//	changed $strSQL - old style	
	if($all)
	{
		$numrows=GetRowCount($strSQL);
	}
}

if(!$all)
{
	LogInfo($strSQL);
	$rs=db_query($strSQL,$conn);
}
else
{
//	 Pagination:
	$nPageSize=0;
	if(@$_REQUEST["records"]=="page" && $numrows)
	{
		$mypage=(integer)@$_SESSION[$strTableName."_pagenumber"];
		$nPageSize=(integer)@$_SESSION[$strTableName."_pagesize"];
		if($numrows<=($mypage-1)*$nPageSize)
			$mypage=ceil($numrows/$nPageSize);
		if(!$nPageSize)
			$nPageSize=$gPageSize;
		if(!$mypage)
			$mypage=1;
		$strSQL.=" limit ".(($mypage-1)*$nPageSize).",".$nPageSize;
	}
	$rs=db_query($strSQL,$conn);
}

$data=db_fetch_array($rs);

if($eventObj->exists("ProcessValuesView"))
	$eventObj->ProcessValuesView($data);

$out="";
$first=true;

$templatefile="";
$fieldsArr = array();
$arr = array();
$arr['fName'] = "gkey";
$arr['viewFormat'] = ViewFormat("gkey", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "eq_nbr";
$arr['viewFormat'] = ViewFormat("eq_nbr", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "line_id";
$arr['viewFormat'] = ViewFormat("line_id", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "depo_id";
$arr['viewFormat'] = ViewFormat("depo_id", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "shipping_agents_id";
$arr['viewFormat'] = ViewFormat("shipping_agents_id", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "shipping_agent_name";
$arr['viewFormat'] = ViewFormat("shipping_agent_name", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "in_visit_type_id";
$arr['viewFormat'] = ViewFormat("in_visit_type_id", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "in_time";
$arr['viewFormat'] = ViewFormat("in_time", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "out_time";
$arr['viewFormat'] = ViewFormat("out_time", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "creator";
$arr['viewFormat'] = ViewFormat("creator", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "created";
$arr['viewFormat'] = ViewFormat("created", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "port_of_origin";
$arr['viewFormat'] = ViewFormat("port_of_origin", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "consignee";
$arr['viewFormat'] = ViewFormat("consignee", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "in_truck_license_nbr";
$arr['viewFormat'] = ViewFormat("in_truck_license_nbr", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "in_trucking_company_group_id";
$arr['viewFormat'] = ViewFormat("in_trucking_company_group_id", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "port_of_destination";
$arr['viewFormat'] = ViewFormat("port_of_destination", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "depo_name";
$arr['viewFormat'] = ViewFormat("depo_name", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "bl_nbr";
$arr['viewFormat'] = ViewFormat("bl_nbr", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "status";
$arr['viewFormat'] = ViewFormat("status", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "category";
$arr['viewFormat'] = ViewFormat("category", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "document_verfied";
$arr['viewFormat'] = ViewFormat("document_verfied", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "survey_pos_id";
$arr['viewFormat'] = ViewFormat("survey_pos_id", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "ctr_position";
$arr['viewFormat'] = ViewFormat("ctr_position", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "iso_code";
$arr['viewFormat'] = ViewFormat("iso_code", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "eq_size";
$arr['viewFormat'] = ViewFormat("eq_size", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "eq_type";
$arr['viewFormat'] = ViewFormat("eq_type", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "ready_to_print_eir";
$arr['viewFormat'] = ViewFormat("ready_to_print_eir", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "print_eir_complete";
$arr['viewFormat'] = ViewFormat("print_eir_complete", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "ready_to_print_eir_out";
$arr['viewFormat'] = ViewFormat("ready_to_print_eir_out", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "print_eir_out_complete";
$arr['viewFormat'] = ViewFormat("print_eir_out_complete", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "sci_seal_filename";
$arr['viewFormat'] = ViewFormat("sci_seal_filename", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "trx_id";
$arr['viewFormat'] = ViewFormat("trx_id", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "payment_release";
$arr['viewFormat'] = ViewFormat("payment_release", $strTableName);
$fieldsArr[] = $arr;
$arr = array();
$arr['fName'] = "tare_weight";
$arr['viewFormat'] = ViewFormat("tare_weight", $strTableName);
$fieldsArr[] = $arr;

$mainTableOwnerID = GetTableData($strTableName,".mainTableOwnerID",'');
$ownerIdValue="";

$pageObject->setGoogleMapsParams($fieldsArr);

while($data)
{
	$xt->assign("show_key1", htmlspecialchars(GetData($data,"gkey", "")));

	$keylink="";
	$keylink.="&key1=".htmlspecialchars(rawurlencode(@$data["gkey"]));

////////////////////////////////////////////
//gkey - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"gkey", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="gkey")
		$ownerIdValue=$value;
	$xt->assign("gkey_value",$value);
	if(!$pageObject->isAppearOnTabs("gkey"))
		$xt->assign("gkey_fieldblock",true);
	else
		$xt->assign("gkey_tabfieldblock",true);
////////////////////////////////////////////
//eq_nbr - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"eq_nbr", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="eq_nbr")
		$ownerIdValue=$value;
	$xt->assign("eq_nbr_value",$value);
	if(!$pageObject->isAppearOnTabs("eq_nbr"))
		$xt->assign("eq_nbr_fieldblock",true);
	else
		$xt->assign("eq_nbr_tabfieldblock",true);
////////////////////////////////////////////
//line_id - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"line_id", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="line_id")
		$ownerIdValue=$value;
	$xt->assign("line_id_value",$value);
	if(!$pageObject->isAppearOnTabs("line_id"))
		$xt->assign("line_id_fieldblock",true);
	else
		$xt->assign("line_id_tabfieldblock",true);
////////////////////////////////////////////
//depo_id - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"depo_id", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="depo_id")
		$ownerIdValue=$value;
	$xt->assign("depo_id_value",$value);
	if(!$pageObject->isAppearOnTabs("depo_id"))
		$xt->assign("depo_id_fieldblock",true);
	else
		$xt->assign("depo_id_tabfieldblock",true);
////////////////////////////////////////////
//shipping_agents_id - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"shipping_agents_id", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="shipping_agents_id")
		$ownerIdValue=$value;
	$xt->assign("shipping_agents_id_value",$value);
	if(!$pageObject->isAppearOnTabs("shipping_agents_id"))
		$xt->assign("shipping_agents_id_fieldblock",true);
	else
		$xt->assign("shipping_agents_id_tabfieldblock",true);
////////////////////////////////////////////
//shipping_agent_name - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"shipping_agent_name", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="shipping_agent_name")
		$ownerIdValue=$value;
	$xt->assign("shipping_agent_name_value",$value);
	if(!$pageObject->isAppearOnTabs("shipping_agent_name"))
		$xt->assign("shipping_agent_name_fieldblock",true);
	else
		$xt->assign("shipping_agent_name_tabfieldblock",true);
////////////////////////////////////////////
//in_visit_type_id - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"in_visit_type_id", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="in_visit_type_id")
		$ownerIdValue=$value;
	$xt->assign("in_visit_type_id_value",$value);
	if(!$pageObject->isAppearOnTabs("in_visit_type_id"))
		$xt->assign("in_visit_type_id_fieldblock",true);
	else
		$xt->assign("in_visit_type_id_tabfieldblock",true);
////////////////////////////////////////////
//in_time - Short Date
	
	$value="";
	$value = ProcessLargeText(GetData($data,"in_time", "Short Date"),"","",MODE_VIEW);
	if($mainTableOwnerID=="in_time")
		$ownerIdValue=$value;
	$xt->assign("in_time_value",$value);
	if(!$pageObject->isAppearOnTabs("in_time"))
		$xt->assign("in_time_fieldblock",true);
	else
		$xt->assign("in_time_tabfieldblock",true);
////////////////////////////////////////////
//out_time - Short Date
	
	$value="";
	$value = ProcessLargeText(GetData($data,"out_time", "Short Date"),"","",MODE_VIEW);
	if($mainTableOwnerID=="out_time")
		$ownerIdValue=$value;
	$xt->assign("out_time_value",$value);
	if(!$pageObject->isAppearOnTabs("out_time"))
		$xt->assign("out_time_fieldblock",true);
	else
		$xt->assign("out_time_tabfieldblock",true);
////////////////////////////////////////////
//creator - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"creator", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="creator")
		$ownerIdValue=$value;
	$xt->assign("creator_value",$value);
	if(!$pageObject->isAppearOnTabs("creator"))
		$xt->assign("creator_fieldblock",true);
	else
		$xt->assign("creator_tabfieldblock",true);
////////////////////////////////////////////
//created - Short Date
	
	$value="";
	$value = ProcessLargeText(GetData($data,"created", "Short Date"),"","",MODE_VIEW);
	if($mainTableOwnerID=="created")
		$ownerIdValue=$value;
	$xt->assign("created_value",$value);
	if(!$pageObject->isAppearOnTabs("created"))
		$xt->assign("created_fieldblock",true);
	else
		$xt->assign("created_tabfieldblock",true);
////////////////////////////////////////////
//port_of_origin - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"port_of_origin", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="port_of_origin")
		$ownerIdValue=$value;
	$xt->assign("port_of_origin_value",$value);
	if(!$pageObject->isAppearOnTabs("port_of_origin"))
		$xt->assign("port_of_origin_fieldblock",true);
	else
		$xt->assign("port_of_origin_tabfieldblock",true);
////////////////////////////////////////////
//consignee - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"consignee", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="consignee")
		$ownerIdValue=$value;
	$xt->assign("consignee_value",$value);
	if(!$pageObject->isAppearOnTabs("consignee"))
		$xt->assign("consignee_fieldblock",true);
	else
		$xt->assign("consignee_tabfieldblock",true);
////////////////////////////////////////////
//in_truck_license_nbr - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"in_truck_license_nbr", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="in_truck_license_nbr")
		$ownerIdValue=$value;
	$xt->assign("in_truck_license_nbr_value",$value);
	if(!$pageObject->isAppearOnTabs("in_truck_license_nbr"))
		$xt->assign("in_truck_license_nbr_fieldblock",true);
	else
		$xt->assign("in_truck_license_nbr_tabfieldblock",true);
////////////////////////////////////////////
//in_trucking_company_group_id - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"in_trucking_company_group_id", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="in_trucking_company_group_id")
		$ownerIdValue=$value;
	$xt->assign("in_trucking_company_group_id_value",$value);
	if(!$pageObject->isAppearOnTabs("in_trucking_company_group_id"))
		$xt->assign("in_trucking_company_group_id_fieldblock",true);
	else
		$xt->assign("in_trucking_company_group_id_tabfieldblock",true);
////////////////////////////////////////////
//port_of_destination - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"port_of_destination", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="port_of_destination")
		$ownerIdValue=$value;
	$xt->assign("port_of_destination_value",$value);
	if(!$pageObject->isAppearOnTabs("port_of_destination"))
		$xt->assign("port_of_destination_fieldblock",true);
	else
		$xt->assign("port_of_destination_tabfieldblock",true);
////////////////////////////////////////////
//depo_name - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"depo_name", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="depo_name")
		$ownerIdValue=$value;
	$xt->assign("depo_name_value",$value);
	if(!$pageObject->isAppearOnTabs("depo_name"))
		$xt->assign("depo_name_fieldblock",true);
	else
		$xt->assign("depo_name_tabfieldblock",true);
////////////////////////////////////////////
//bl_nbr - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"bl_nbr", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="bl_nbr")
		$ownerIdValue=$value;
	$xt->assign("bl_nbr_value",$value);
	if(!$pageObject->isAppearOnTabs("bl_nbr"))
		$xt->assign("bl_nbr_fieldblock",true);
	else
		$xt->assign("bl_nbr_tabfieldblock",true);
////////////////////////////////////////////
//status - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"status", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="status")
		$ownerIdValue=$value;
	$xt->assign("status_value",$value);
	if(!$pageObject->isAppearOnTabs("status"))
		$xt->assign("status_fieldblock",true);
	else
		$xt->assign("status_tabfieldblock",true);
////////////////////////////////////////////
//category - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"category", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="category")
		$ownerIdValue=$value;
	$xt->assign("category_value",$value);
	if(!$pageObject->isAppearOnTabs("category"))
		$xt->assign("category_fieldblock",true);
	else
		$xt->assign("category_tabfieldblock",true);
////////////////////////////////////////////
//document_verfied - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"document_verfied", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="document_verfied")
		$ownerIdValue=$value;
	$xt->assign("document_verfied_value",$value);
	if(!$pageObject->isAppearOnTabs("document_verfied"))
		$xt->assign("document_verfied_fieldblock",true);
	else
		$xt->assign("document_verfied_tabfieldblock",true);
////////////////////////////////////////////
//survey_pos_id - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"survey_pos_id", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="survey_pos_id")
		$ownerIdValue=$value;
	$xt->assign("survey_pos_id_value",$value);
	if(!$pageObject->isAppearOnTabs("survey_pos_id"))
		$xt->assign("survey_pos_id_fieldblock",true);
	else
		$xt->assign("survey_pos_id_tabfieldblock",true);
////////////////////////////////////////////
//ctr_position - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"ctr_position", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="ctr_position")
		$ownerIdValue=$value;
	$xt->assign("ctr_position_value",$value);
	if(!$pageObject->isAppearOnTabs("ctr_position"))
		$xt->assign("ctr_position_fieldblock",true);
	else
		$xt->assign("ctr_position_tabfieldblock",true);
////////////////////////////////////////////
//iso_code - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"iso_code", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="iso_code")
		$ownerIdValue=$value;
	$xt->assign("iso_code_value",$value);
	if(!$pageObject->isAppearOnTabs("iso_code"))
		$xt->assign("iso_code_fieldblock",true);
	else
		$xt->assign("iso_code_tabfieldblock",true);
////////////////////////////////////////////
//eq_size - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"eq_size", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="eq_size")
		$ownerIdValue=$value;
	$xt->assign("eq_size_value",$value);
	if(!$pageObject->isAppearOnTabs("eq_size"))
		$xt->assign("eq_size_fieldblock",true);
	else
		$xt->assign("eq_size_tabfieldblock",true);
////////////////////////////////////////////
//eq_type - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"eq_type", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="eq_type")
		$ownerIdValue=$value;
	$xt->assign("eq_type_value",$value);
	if(!$pageObject->isAppearOnTabs("eq_type"))
		$xt->assign("eq_type_fieldblock",true);
	else
		$xt->assign("eq_type_tabfieldblock",true);
////////////////////////////////////////////
//ready_to_print_eir - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"ready_to_print_eir", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="ready_to_print_eir")
		$ownerIdValue=$value;
	$xt->assign("ready_to_print_eir_value",$value);
	if(!$pageObject->isAppearOnTabs("ready_to_print_eir"))
		$xt->assign("ready_to_print_eir_fieldblock",true);
	else
		$xt->assign("ready_to_print_eir_tabfieldblock",true);
////////////////////////////////////////////
//print_eir_complete - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"print_eir_complete", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="print_eir_complete")
		$ownerIdValue=$value;
	$xt->assign("print_eir_complete_value",$value);
	if(!$pageObject->isAppearOnTabs("print_eir_complete"))
		$xt->assign("print_eir_complete_fieldblock",true);
	else
		$xt->assign("print_eir_complete_tabfieldblock",true);
////////////////////////////////////////////
//ready_to_print_eir_out - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"ready_to_print_eir_out", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="ready_to_print_eir_out")
		$ownerIdValue=$value;
	$xt->assign("ready_to_print_eir_out_value",$value);
	if(!$pageObject->isAppearOnTabs("ready_to_print_eir_out"))
		$xt->assign("ready_to_print_eir_out_fieldblock",true);
	else
		$xt->assign("ready_to_print_eir_out_tabfieldblock",true);
////////////////////////////////////////////
//print_eir_out_complete - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"print_eir_out_complete", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="print_eir_out_complete")
		$ownerIdValue=$value;
	$xt->assign("print_eir_out_complete_value",$value);
	if(!$pageObject->isAppearOnTabs("print_eir_out_complete"))
		$xt->assign("print_eir_out_complete_fieldblock",true);
	else
		$xt->assign("print_eir_out_complete_tabfieldblock",true);
////////////////////////////////////////////
//sci_seal_filename - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"sci_seal_filename", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="sci_seal_filename")
		$ownerIdValue=$value;
	$xt->assign("sci_seal_filename_value",$value);
	if(!$pageObject->isAppearOnTabs("sci_seal_filename"))
		$xt->assign("sci_seal_filename_fieldblock",true);
	else
		$xt->assign("sci_seal_filename_tabfieldblock",true);
////////////////////////////////////////////
//trx_id - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"trx_id", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="trx_id")
		$ownerIdValue=$value;
	$xt->assign("trx_id_value",$value);
	if(!$pageObject->isAppearOnTabs("trx_id"))
		$xt->assign("trx_id_fieldblock",true);
	else
		$xt->assign("trx_id_tabfieldblock",true);
////////////////////////////////////////////
//payment_release - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"payment_release", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="payment_release")
		$ownerIdValue=$value;
	$xt->assign("payment_release_value",$value);
	if(!$pageObject->isAppearOnTabs("payment_release"))
		$xt->assign("payment_release_fieldblock",true);
	else
		$xt->assign("payment_release_tabfieldblock",true);
////////////////////////////////////////////
//tare_weight - 
	
	$value="";
	$value = ProcessLargeText(GetData($data,"tare_weight", ""),"","",MODE_VIEW);
	if($mainTableOwnerID=="tare_weight")
		$ownerIdValue=$value;
	$xt->assign("tare_weight_value",$value);
	if(!$pageObject->isAppearOnTabs("tare_weight"))
		$xt->assign("tare_weight_fieldblock",true);
	else
		$xt->assign("tare_weight_tabfieldblock",true);

/*$jsKeysObj = 'window.recKeysObj = {';
	$jsKeysObj .= "'".jsreplace("gkey")."': '".(jsreplace(@$data["gkey"]))."', ";
$jsKeysObj = substr($jsKeysObj, 0, strlen($jsKeysObj)-2);
$jsKeysObj .= '};';
$pageObject->AddJsCode($jsKeysObj);	
*/
/////////////////////////////////////////////////////////////
if($pageObject->isShowDetailTables && !isMobile())
{
	if(count($dpParams['ids']))
	{
		$xt->assign("detail_tables",true);
		include('classes/listpage.php');
		include('classes/listpage_embed.php');
		include('classes/listpage_dpinline.php');
	}
	
	$dControlsMap = array();
	
	for($d=0;$d<count($dpParams['ids']);$d++)
	{
		$options = array();
		//array of params for classes
		$options["mode"] = LIST_DETAILS;
		$options["pageType"] = PAGE_LIST;
		$options["masterPageType"] = PAGE_VIEW;
		$options["mainMasterPageType"] = PAGE_VIEW;
		$options['masterTable'] = "equipment_uses";
		$options['firstTime'] = 1;
		
		$strTableName = $dpParams['strTableNames'][$d];
		include("include/".GetTableURL($strTableName)."_settings.php");
		if(!CheckSecurity(@$_SESSION["_".$strTableName."_OwnerID"],"Search"))
		{
			$strTableName = "equipment_uses";
			continue;
		}
		
		$options['xt'] = new Xtempl();
		$options['id'] = $dpParams['ids'][$d];
		$options['flyId'] = $pageObject->genId()+1;
		$mkr = 1;
		foreach($mKeys[$strTableName] as $mk)
		{
			$options['masterKeysReq'][$mkr++] = $data[$mk];
		}
		$listPageObject = ListPage::createListPage($strTableName, $options);
		// prepare code
		$listPageObject->prepareForBuildPage();
		// show page
		if(!$pdf && $listPageObject->isDispGrid())
		{
			//add detail settings to master settings
			$listPageObject->fillSetCntrlMaps();
			$pageObject->jsSettings['tableSettings'][$strTableName]	= $listPageObject->jsSettings['tableSettings'][$strTableName];
			$dControlsMap[$strTableName] = $listPageObject->controlsMap;
			foreach($listPageObject->jsSettings['global']['shortTNames'] as $keySet=>$val)
			{
				if(!array_key_exists($keySet,$pageObject->settingsMap["globalSettings"]['shortTNames']))
				{
					$pageObject->settingsMap["globalSettings"]['shortTNames'][$keySet] = $val;
				}
			}
			
			//Add detail's js files to master's files
			$pageObject->copyAllJSFiles($listPageObject->grabAllJSFiles());
			
			//Add detail's css files to master's files
			$pageObject->copyAllCSSFiles($listPageObject->grabAllCSSFiles());
		}
		//$xt->assign("displayDetailTable_".GoodFieldName($strTableName), array("func" => "showDetailTable","params" => array("dpObject" => $listPageObject, "dpParams" => $strTableName)));
		$xtParams = array("method"=>'showPage', "params"=> false);
		$xtParams['object'] = $listPageObject;
		$xt->assign("displayDetailTable_".GoodFieldName($listPageObject->tName), $xtParams);
		
		$pageObject->controlsMap['dpTablesParams'][] = array('tName'=>$strTableName, 'id'=>$options['id']);
	}
	$pageObject->controlsMap['dControlsMap'] = $dControlsMap;
	$strTableName = "equipment_uses";
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Begin prepare for Next Prev button
if(!@$_SESSION[$strTableName."_noNextPrev"] && !$inlineview && !$pdf)
{
	$pageObject->getNextPrevRecordKeys($data,"Search",$next,$prev);
}
//End prepare for Next Prev button
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


if ($pageObject->googleMapCfg['isUseGoogleMap'])
{
	$pageObject->initGmaps();
}

$pageObject->addCommonJs();

//fill tab groups name and sections name to controls
$pageObject->fillCntrlTabGroups();

if(!$inlineview)
{
	$pageObject->body["begin"].="<script type=\"text/javascript\" src=\"include/loadfirst.js\"></script>\r\n";
		$pageObject->body["begin"].= "<script type=\"text/javascript\" src=\"include/lang/".getLangFileName(mlang_getcurrentlang()).".js\"></script>";		
	
	$pageObject->jsSettings['tableSettings'][$strTableName]["keys"] = $keys;
	$pageObject->jsSettings['tableSettings'][$strTableName]["prevKeys"] = $prev;
	$pageObject->jsSettings['tableSettings'][$strTableName]["nextKeys"] = $next; 
	
	// assign body end
	$pageObject->body['end'] = array();
	$pageObject->body['end']["method"] = "assignBodyEnd";
	$pageObject->body['end']["object"] = &$pageObject;
	
	$xt->assign("body",$pageObject->body);
	$xt->assign("flybody",true);
}
else
{
	$xt->assign("footer",false);
	$xt->assign("header",false);
	$xt->assign("flybody",$pageObject->body);
	$xt->assign("body",true);
	$xt->assign("pdflink_block",false);
	
	$pageObject->fillSetCntrlMaps();
	
	$returnJSON['controlsMap'] = $pageObject->controlsHTMLMap;
	$returnJSON['settings'] = $pageObject->jsSettings;
}
$xt->assign("style_block",true);
$xt->assign("stylefiles_block",true);

$editlink="";
$editkeys=array();
	$editkeys["editid1"]=postvalue("editid1");
foreach($editkeys as $key=>$val)
{
	if($editlink)
		$editlink.="&";
	$editlink.=$key."=".$val;
}
$xt->assign("editlink_attrs","id=\"editLink".$id."\" name=\"editLink".$id."\" onclick=\"window.location.href='equipment_uses_edit.php?".$editlink."'\"");

$strPerm = GetUserPermissions($strTableName);
if(CheckSecurity($ownerIdValue,"Edit") && !$inlineview && strpos($strPerm, "E")!==false)
	$xt->assign("edit_button",true);
else	
	$xt->assign("edit_button",false);

if(!$pdf && !$all && !$inlineview)
{
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Begin show Next Prev button
    $nextlink=$prevlink="";
	if(count($next))
    {
		$xt->assign("next_button",true);
	 		$nextlink .="editid1=".htmlspecialchars(rawurlencode($next[1]));
		$xt->assign("nextbutton_attrs","id=\"nextButton".$id."\"");
	}
	else 
		$xt->assign("next_button",false);
	if(count($prev))
	{
		$xt->assign("prev_button",true);
			$prevlink .="editid1=".htmlspecialchars(rawurlencode($prev[1]));
		$xt->assign("prevbutton_attrs","id=\"prevButton".$id."\"");
	}
    else 
		$xt->assign("prev_button",false);
//End show Next Prev button
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$xt->assign("back_button",true);
	$xt->assign("backbutton_attrs","id=\"backButton".$id."\"");
}

$oldtemplatefile=$templatefile;
$templatefile = "equipment_uses_view.htm";

if(!$all)
{
	if($eventObj->exists("BeforeShowView"))
		$eventObj->BeforeShowView($xt,$templatefile,$data);
	
	if(!$pdf)
	{
		if(!$inlineview)
			$xt->display($templatefile);
		else{
				$xt->load_template($templatefile);
				$returnJSON['html'] = $xt->fetch_loaded('style_block').$xt->fetch_loaded('body');
				$returnJSON['idStartFrom'] = $id+1;
				echo (my_json_encode($returnJSON)); 
			}
	}
	break;
}
}


?>
