<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Customer extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Customer_Model');
        $this->load->library('lib_auth');
        $this->load->model('Log_Model');
    }

    public function index()
    {
        $this->lib_auth->check('yes');
        $data['page_title'] = APP_NAME . " | All Customer";

        if (isset($_GET['hal']))
            $hal = $_GET['hal'];
        else
            $hal = '';

        $dataPerhalaman = 30;
        ($hal == '') ? $nohalaman = 1 : $nohalaman = $hal;
        $offset = ($nohalaman - 1) * $dataPerhalaman;
        $off = abs((int) $offset);
        
        $jumlahData = count($this->Customer_Model->get_all_data("customers"));
        
        $data['paginator'] = $this->Customer_Model->page($jumlahData, $dataPerhalaman, $hal);
        
        $data['datas'] = $this->Customer_Model->get_all_data_tabel("customers", $dataPerhalaman, $off);

        $this->load->view('data/all_customer_v', $data);
    }

    public function add_new()
    {
        $data['page_title'] = APP_NAME . " | Add New Customer";

        $this->load->library('form_validation');

        $this->load->view('data/add_new_customer_v', $data);
    }

    public function save()
    {
        $input = array(
            "tax_id" => $this->input->post("tax_id"),
            "name" => $this->input->post("name"),
            "addresss" => "",
            "city" => "",
            "postal_code" => "",
            "created" => "" . date('Y-m-d H:i:s') . "",
            "creator" => $_SESSION[SESSION_NAME]['unique_id'],
            "changed" => "0000-00-00 00:00:00",
            "changer" => " ",
        );
        $edit = $this->input->post("edit");
        if ($edit)
        {
            $id = $this->input->post("id");
            $record = $this->Customer_Model->update("customers", $id, $input, "customer_id");

            //$this->Log_Model->insert_log($_SESSION[SESSION_NAME]['uid'], "update", "customer", $input);

            redirect(base_url() . "customer");
        }
        else
        {
            $record = $this->Customer_Model->insert("customers", $input);
            //$this->Log_Model->insert_log($_SESSION[SESSION_NAME]['uid'], "add", "customer", $input);
        }
        redirect(base_url() . "customer");
    }

    public function delete()
    {
        $id_product = $this->uri->segment(3, '');

        if (isset($id_product))
        {
            $product = $this->Customer_Model->get_single("customers", $id_product, "customer_id");

            if ($product)
            {
                $this->Customer_Model->delete("customers", $id_product, "customer_id");
                
                //$this->Log_Model->insert_log($_SESSION[SESSION_NAME]['uid'], "delete", "customer", $id_product);
            }
        }
        redirect(base_url() . "customer");
    }

    public function view()
    {
        $id_product = $this->uri->segment(3, '');

        $data['edit'] = TRUE;
        if (isset($id_product))
        {
            $data['datas'] = $this->Customer_Model->get_single("customers", $id_product, "customer_id");
        }
        else
        {
            redirect(base_url() . "customer");
        }
        $data['page_title'] = APP_NAME . " | Edit Customer";

        $this->load->library('form_validation');

        $this->load->view('data/edit_customer_v', $data);
    }

}