<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Port extends CI_Controller {

    public function __construct()
    {

        parent::__construct();

        $this->load->model('Port_Model');
        $this->load->library('lib_auth');
        $this->load->model('Log_Model');
    }

    public function index()
    {
        $this->lib_auth->check('yes');
        $data['page_title'] = APP_NAME . " | All Port Origin / Destination";

        if (isset($_GET['hal']))
            $hal = $_GET['hal'];
        else
            $hal = '';

        $dataPerhalaman = 100;
        ($hal == '') ? $nohalaman = 1 : $nohalaman = $hal;
        $offset = ($nohalaman - 1) * $dataPerhalaman;
        $off = abs((int) $offset);
        
        $jumlahData = count($this->Port_Model->get_all_data("portcodes"));
        
        $data['paginator'] = $this->Port_Model->page($jumlahData, $dataPerhalaman, $hal);
        
        $data['datas'] = $this->Port_Model->get_all_data_tabel("portcodes", $dataPerhalaman, $off);

        $this->load->view('data/all_port_v', $data);
    }

    public function add_new()
    {
        $data['page_title'] = APP_NAME . " | Add New Origin/Destination";

        $this->load->library('form_validation');

        $this->load->view('data/add_new_port_v', $data);
    }

    public function save()
    {
        $input = array(
            "id" => str_replace(" ", "", strtoupper($this->input->post("id"))),
            "name" => str_replace(" ", "", strtoupper($this->input->post("name"))),
            "code" => str_replace(" ", "", strtoupper($this->input->post("code"))),
            "country" => strtoupper($this->input->post("country")),
        );
        $edit = $this->input->post("edit");
        if ($edit)
        {
            $id = $this->input->post("id");
            $record = $this->Port_Model->update("portcodes", $id, $input, "id");

            redirect(base_url() . "port");
        }
        else
        {
            $record = $this->Port_Model->insert("portcodes", $input);
            redirect(base_url() . "port");
        }
    }

    public function delete()
    {
        $id_product = $this->uri->segment(3, '');

        if (isset($id_product))
        {
            $product = $this->Port_Model->delete("portcodes", $id_product, "id");

            if ($product)
            {
                $this->Port_Model->delete("portcodes", $id_product, "id");
            }
        }
        redirect(base_url() . "port");
    }

    public function view()
    {
        $id_product = $this->uri->segment(3, '');

        $data['edit'] = TRUE;
        if (isset($id_product))
        {
            $data['datas'] = $this->Port_Model->get_single("portcodes", $id_product, "id");
        }
        else
        {
            redirect(base_url() . "port");
        }
        $data['page_title'] = APP_NAME . " | Edit Port Origin/Destination";

        $this->load->library('form_validation');

        $this->load->view('data/edit_depo_v', $data);
    }

}