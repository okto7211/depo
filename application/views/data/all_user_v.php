<?php echo $this->load->view('backend/inc_header_v') ?>
<?php echo $this->load->view('backend/inc_top_v') ?>
<div class="row">
    <div class="wrapper_content">
        <?php echo $this->load->view('backend/inc_sidebar_v') ?>
        <div id="content" class="nine columns">
            <div class="row">
                <div class="twelve columns box_gray box_dashboard box_corner">
                    <h2>Users</h2>
                    <div class="divider"></div>
                    <table class='striped rounded'>
                        <thead>
                            <tr>
                                <th width="8%">No</th>
                                <th width="25%">Unique ID</th>
                                <th width="25%">Name</th>
                                <th width="25%">As</th>
                                <th width="25%">Depo</th>
                                <th width="30%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $no = 1;
                            if (!empty($datas))
                            {
                                foreach ($datas as $data)
                                {
                                    $edit = "<a href=" . base_url('user/view/' . str_replace(" ", "_", $data['uid'])) . ">View</a>";
                                    $delete = "<a href=" . base_url('user/delete/' . str_replace(" ", "_", $data['uid'])) . " onclick='return confirm('Are you sure delete this voucher?')'>Delete</a>";

                                    if ($data['role_id'] !== "superadmin")
                                    {

                                        $action = $edit . " " . $delete;
                                    }
                                    else
                                    {
                                        $action = "";
                                    }
                                    echo "<tr>" .
                                    "<td>" . $no . "</td>" .
                                    "<td>" . $data['unique_id'] . "</td>" .
                                    "<td>" . $data['name'] . "</td>" .
                                    "<td>" . $data['role_id'] . "</td>" .
                                    "<td>" . $data['depo_id'] . "</td>" .
                                    "<td>" . $action . "</td>" .
                                    "</tr>";
                                    $no++;
                                }
                            }
                            else
                            {
                                echo "<tr><td colspan='5'><div align='center'>No Depo</div></td></tr>";
                            }
                            ?>							
                        </tbody>
                    </table>
                    <div class="clear"></div><br/>
                    <!--<div class="pagination" align="center"><?php echo $pagination ?></div>-->
                </div>
            </div>			
        </div>		
    </div>		
</div>
<?php echo $this->load->view('backend/inc_footer_v') ?>		
