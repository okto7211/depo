<link rel="stylesheet" href="<?php echo BASE_ASSET ?>css/gumby.css">
<link rel="stylesheet" href="<?php echo BASE_ASSET ?>css/backend.css">
<link rel="stylesheet" href="<?php echo BASE_ASSET ?>js/fancybox/jquery.fancybox.css">
<link rel="stylesheet" href="<?php echo BASE_ASSET ?>js/nprogress.css">	
<link rel="stylesheet" href="<?php echo BASE_ASSET ?>js/pickadate.default.css">	
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/libs/modernizr-2.6.2.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/libs/gumby.min.js"></script>	
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/h5f.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/fancybox/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/nprogress.js"></script>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/jquery.ajax.progress.js"></script>	
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/pickadate.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/backend.js"></script>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ASSET ?>js/jquery.timepicker.css" />
<table border="1">
    <tr>
        <td><div align="center">No.</div></td>
        <td><div align="center">ISO CODE</div></td>
        <td><div align="center">Size</div></td>
        <td><div align="center">Type</div></td>
        <td><div align="center">Action</div></td>
    </tr>
    <?php
    $index = 1;
    foreach ($iso as $v)
    {
        ?>
        <tr>
            <td><div align="center"><?= $index; ?></div></td>
            <td><div align="center"><?= $v['iso_code_id']; ?></div></td>
            <td><div align="center"><?= $v['size']; ?></div></td>
            <td><div align="center"><?= $v['type']; ?></div></td>
            <td><div align="center"><a href="javascript:pilih('<?= $v['iso_code_id']; ?>', '<?= $v['size']; ?>', '<?= $v['type']; ?>')">Select</a></div></td>
        </tr>
        <?php
        $index++;
    }
    ?>
</table>
<script type="text/javascript">
    function pilih(iso, size, type) {

        self.opener.document.f1.iso_code.value = iso;
        self.opener.document.f1.size.value = size;
        self.opener.document.f1.type.value = type;

        window.close();
    }
</script>
