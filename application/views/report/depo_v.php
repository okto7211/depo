<?php echo $this->load->view('backend/inc_header_v') ?>
<?php echo $this->load->view('backend/inc_top_v') ?>
<div class="row">
    <div class="wrapper_content">
        <?php echo $this->load->view('backend/inc_sidebar_v') ?>
        <div id="content" class="nine columns">
            <div class="row">
                <div class="twelve columns box_gray box_dashboard box_corner">
                    <h2>Report Depo</h2>
                    <div class="divider"></div>
                     <form name="f1" id="f1" class="field" method="get" action="<?php echo base_url('report/depo') ?>" enctype="multipart/form-data">
                        <span>Depo</span>
                        <select id="depo_id" name ="depo_id" disabled="disabled" style="padding: 6px 5px;border-radius: 4px;border: 1px solid #d8d8d8;">
                            <option value="<?= htmlspecialchars($depo['id']); ?>"><?= htmlspecialchars($depo['name']); ?> (<?= htmlspecialchars($depo['id']); ?>)</option>
                        </select>
                        <br/>
                        <span>Bulan</span>
                        <select id="month" name ="month" style="padding: 6px 5px;border-radius: 4px;border: 1px solid #d8d8d8;">
                            <option value="01" <?= $month == 1 ? "selected=''" : ""; ?>>Januari</option>
                            <option value="02" <?= $month == 2 ? "selected=''" : ""; ?>>Pebruari</option>
                            <option value="03" <?= $month == 3 ? "selected=''" : ""; ?>>Maret</option>
                            <option value="04" <?= $month == 4 ? "selected=''" : ""; ?>>April</option>
                            <option value="05" <?= $month == 5 ? "selected=''" : ""; ?>>Mei</option>
                            <option value="06" <?= $month == 6 ? "selected=''" : ""; ?>>Juni</option>
                            <option value="07" <?= $month == 7 ? "selected=''" : ""; ?>>Juli</option>
                            <option value="08" <?= $month == 8 ? "selected=''" : ""; ?>>Agustus</option>
                            <option value="09" <?= $month == 9 ? "selected=''" : ""; ?>>September</option>
                            <option value="10" <?= $month == 10 ? "selected=''" : ""; ?>>Oktober</option>
                            <option value="11" <?= $month == 11 ? "selected=''" : ""; ?>>Nopember</option>
                            <option value="12" <?= $month == 12 ? "selected=''" : ""; ?>>Desember</option>
                        </select>
                        <br/>
                        <span>Tahun</span>
                        <select id="year" name ="year" style="padding: 6px 5px;border-radius: 4px;border: 1px solid #d8d8d8;">
                            <option value="2012" <?= $year == 2012 ? "selected=''" : ""; ?>>2012</option>
                            <option value="2013" <?= $year == 2013 ? "selected=''" : ""; ?>>2013</option>
                            <option value="2014" <?= $year == 2014 ? "selected=''" : ""; ?>>2014</option>
                            <option value="2015" <?= $year == 2015 ? "selected=''" : ""; ?>>2015</option>
                            <option value="2016" <?= $year == 2016 ? "selected=''" : ""; ?>>2016</option>
                            <option value="2017" <?= $year == 2017 ? "selected=''" : ""; ?>>2017</option>
                        </select>
                        <input type="submit"class="submit" name="find" id="find" style="width: 100px;height: 35px;float: right;border-radius: 2px" value="Find">
                     </form>
                     <div class="divider"></div>
                     <table class='striped rounded'>
                        <thead>
                            <tr>
                                <th width="20%">Eq Number</th>
                                <th width="20%">Position</th>
                                <th width="10%">Origin</th>
                                <th width="10%">Destination</th>
                                <th width="10%">ISO Code</th>
                                <th width="5%">Size</th>
                                <th width="5%">Type</th>
                                <th width="50%"><div align="center">Action</a></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($datas))
                            {
                                foreach ($datas as $data)
                                {
                                    echo "<tr>" .
                                    "<td>" . $data['eq_nbr'] . "</td>" .
                                    "<td>" . $data['ctr_position'] . "</td>" .
                                    "<td>" . $data['port_of_origin'] . "</td>" .
                                    "<td>" . $data['port_of_destination'] . "</td>" .
                                    "<td>" . $data['iso_code'] . "</td>" .
                                    "<td>" . $data['eq_size'] . "</td>" .
                                    "<td>" . $data['eq_type'] . "</td>" .
                                    "<td><div align='center'>";
                                    ?>
                                    <a href="javascript:void(0)" onclick="printPdf('<?=$data['eq_nbr'] ;?>')"><input type="button" class="" value="PDF"></a>
                                    <a href="javascript:void(0)" onclick="printEir('<?=$data['eq_nbr'] ;?>')"><input type="button" class="" value="EIR"></a>
                                <?php
                                    echo "</div></td></tr>";
                                }
                            }
                            else
                            {
                                echo "<tr><td colspan='8'><div align='center'>No Equipment</div></td></tr>";
                            }
                            ?>							
                        </tbody>
                    </table>
                    <div class="clear"></div>
                    <div align="center"><?php echo $paginator; ?></div>
                </div>
            </div>			
        </div>		
    </div>		
</div>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/jquery.popupWindow.js"></script>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/ckeditor/adapters/jquery.js"></script>	
<?php include('asset/js/ckeditor/ckinit.js.php'); ?>
<?php echo $this->load->view('backend/inc_footer_v') ?>		
<script type= "text/javascript">
function printPdf(eq_nbr){
    var w = 100;
    var h = 100;
    var left = (screen.width / 2) - (w / 4);
    var top = (screen.height / 2) - (h / 2);
    window.open("<?= BASE_URL ?>equipment/print_pdf?eq_nbr="+eq_nbr, "_blank", "toolbar=yes, location=no, directories=no, status=no, menubar=yes, scrollbars=yes, resizable=no, copyhistory=yes, width=1000px, height=600px, top=120, left=200");

}
function printEir(eq_nbr){
    var w = 100;
    var h = 100;
    var left = (screen.width / 2) - (w / 4);
    var top = (screen.height / 2) - (h / 2);
    window.open("<?= BASE_URL ?>equipment/print_eir?eq_nbr="+eq_nbr, "_blank", "toolbar=yes, location=no, directories=no, status=no, menubar=yes, scrollbars=yes, resizable=no, copyhistory=yes, width=1000px, height=600px, top=120, left=200");
    
}
</script>
