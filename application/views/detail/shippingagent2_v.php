<link rel="stylesheet" href="<?php echo BASE_ASSET ?>css/gumby.css">
<link rel="stylesheet" href="<?php echo BASE_ASSET ?>css/backend.css">
<link rel="stylesheet" href="<?php echo BASE_ASSET ?>js/fancybox/jquery.fancybox.css">
<link rel="stylesheet" href="<?php echo BASE_ASSET ?>js/nprogress.css">	
<link rel="stylesheet" href="<?php echo BASE_ASSET ?>js/pickadate.default.css">	
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/libs/modernizr-2.6.2.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/libs/gumby.min.js"></script>	
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/h5f.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/fancybox/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/nprogress.js"></script>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/jquery.ajax.progress.js"></script>	
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/pickadate.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/jquery.inputmask.bundle.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/backend.js"></script>
<script type="text/javascript" src="<?php echo BASE_ASSET ?>js/jquery.timepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo BASE_ASSET ?>js/jquery.timepicker.css" />
<table border="1">
    <tr>
        <td><div align="center">No.</div></td>
        <td><div align="center">Tax Id</div></td>
        <td><div align="center">Name</div></td>
        <td><div align="center">Action</div></td>
    </tr>
    <?php
    $index = 1;
    foreach ($shippingagents as $v)
    {
        ?>
        <tr>
            <td><div align="center"><?= $index; ?></div></td>
            <td><div align="center"><?= $v['id']; ?></div></td>
            <td><div align="center"><?= $v['name']; ?></div></td>
            <td><div align="center"><a href="javascript:pilih('<?= str_replace(" ", "_",$v['id']); ?>','<?= $v['name']; ?>')">Select</a></div></td>
        </tr>
        <?php
        $index++;
    }
    ?>
</table>
<script type="text/javascript">
function pilih(id, name) {
	self.opener.document.f1.shipping_id.value = id;
	self.opener.document.f1.shipping_name.value = name;
    
	window.close();
}
</script>
