<?php
@ini_set("display_errors","1");
@ini_set("display_startup_errors","1");

include("include/dbcommon.php");
header("Expires: Thu, 01 Jan 1970 00:00:01 GMT"); 

include("include/invoice_variables.php");

$mode=postvalue("mode");

if(!@$_SESSION["UserID"])
{ 
	return;
}
if(!CheckSecurity(@$_SESSION["_".$strTableName."_OwnerID"],"Search"))
{
	return;
}

include('include/xtempl.php');
$xt = new Xtempl();

$layout = new TLayout("detailspreview","BoldOrange","MobileOrange");
$layout->blocks["bare"] = array();
$layout->containers["0"] = array();

$layout->containers["0"][] = array("name"=>"detailspreviewheader","block"=>"","substyle"=>1);


$layout->skins["0"] = "empty";
$layout->blocks["bare"][] = "0";
$layout->containers["0"] = array();

$layout->containers["0"][] = array("name"=>"detailspreviewdetailsfount","block"=>"","substyle"=>1);


$layout->containers["0"][] = array("name"=>"detailspreviewdispfirst","block"=>"display_first","substyle"=>1);


$layout->skins["0"] = "empty";
$layout->blocks["bare"][] = "0";
$layout->containers["detailspreviewgrid"] = array();

$layout->containers["detailspreviewgrid"][] = array("name"=>"detailspreviewfields","block"=>"details_data","substyle"=>1);


$layout->skins["detailspreviewgrid"] = "grid";
$layout->blocks["bare"][] = "detailspreviewgrid";$page_layouts["invoice_detailspreview"] = $layout;


$recordsCounter = 0;

//	process masterkey value
$mastertable=postvalue("mastertable");
if($mastertable!="")
{
	$_SESSION[$strTableName."_mastertable"]=$mastertable;
//	copy keys to session
	$i=1;
	while(isset($_REQUEST["masterkey".$i]))
	{
		$_SESSION[$strTableName."_masterkey".$i]=$_REQUEST["masterkey".$i];
		$i++;
	}
	if(isset($_SESSION[$strTableName."_masterkey".$i]))
		unset($_SESSION[$strTableName."_masterkey".$i]);
}
else
	$mastertable=$_SESSION[$strTableName."_mastertable"];

//$strSQL = $gstrSQL;

if($mastertable=="transaction")
{
	$where ="";
		$where.= GetFullFieldName("trx_id")."=".make_db_value("trx_id",$_SESSION[$strTableName."_masterkey1"]);
}


$str = SecuritySQL("Search");
if(strlen($str))
	$where.=" and ".$str;
$strSQL = gSQLWhere($where);

$strSQL.=" ".$gstrOrderBy;

$rowcount=gSQLRowCount($where);

$xt->assign("row_count",$rowcount);
if ( $rowcount ) {
	$xt->assign("details_data",true);
	$rs=db_query($strSQL,$conn);

	$display_count=10;
	if($mode=="inline")
		$display_count*=2;
	if($rowcount>$display_count+2)
	{
		$xt->assign("display_first",true);
		$xt->assign("display_count",$display_count);
	}
	else
		$display_count = $rowcount;

	$rowinfo=array();
		
	while (($data = db_fetch_array($rs)) && $recordsCounter<$display_count) {
		$recordsCounter++;
		$row=array();
		$keylink="";
		$keylink.="&key1=".htmlspecialchars(rawurlencode(@$data["trx_id"]));

	
	//	invoice_nbr - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"invoice_nbr", ""),"field=invoice%5Fnbr".$keylink,"",MODE_PRINT);
			$row["invoice_nbr_value"]=$value;
	//	trx_id - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"trx_id", ""),"field=trx%5Fid".$keylink,"",MODE_PRINT);
			$row["trx_id_value"]=$value;
	//	iso_code - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"iso_code", ""),"field=iso%5Fcode".$keylink,"",MODE_PRINT);
			$row["iso_code_value"]=$value;
	//	qty_of_20 - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"qty_of_20", ""),"field=qty%5Fof%5F20".$keylink,"",MODE_PRINT);
			$row["qty_of_20_value"]=$value;
	//	qty_of_40 - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"qty_of_40", ""),"field=qty%5Fof%5F40".$keylink,"",MODE_PRINT);
			$row["qty_of_40_value"]=$value;
	//	total - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"total", ""),"field=total".$keylink,"",MODE_PRINT);
			$row["total_value"]=$value;
	//	created - Short Date
		    $value="";
				$value = ProcessLargeText(GetData($data,"created", "Short Date"),"field=created".$keylink,"",MODE_PRINT);
			$row["created_value"]=$value;
	//	creator - 
		    $value="";
				$value = ProcessLargeText(GetData($data,"creator", ""),"field=creator".$keylink,"",MODE_PRINT);
			$row["creator_value"]=$value;
	$rowinfo[]=$row;
	}
	$xt->assign_loopsection("details_row",$rowinfo);
} else {
}
$xt->display("invoice_detailspreview.htm");
if($mode!="inline"){
	echo "counterSeparator".postvalue("counter");
	$layout = GetPageLayout(GoodFieldName($strTableName), 'detailspreview');
	if($layout)
	{
		$rtl = $xt->getReadingOrder() == 'RTL' ? 'RTL' : '';
		echo "counterSeparator"."styles/".$layout->style."/style".$rtl
			."counterSeparator"."pagestyles/".$layout->name.$rtl
			."counterSeparator".$layout->style." page-".$layout->name;
	}	
}	
?>